(function(module){
	'use strict';
	var util = require('./util');
	var boosts = [{
		name: 'Wow Clickboost',
		action: 'click',
		duration: 0,
		value: 0.2
	},{
		name: 'Much extra',
		action: 'extra',
		duration: 0,
		value: 1.25
	},{
		name: 'Wow much click',
		action: 'click',
		duration: 18,
		value: 5
	},{
		name: 'Much negative',
		action: 'extra',
		duration: 0,
		value: 0.75
	}];
	module.exports.generate = function(callback){
		console.log('Calling generate');
		var delay = Math.floor((1000*30)+(1000*30)*Math.random());
		var boost = util.randomFromArray(boosts);
		setTimeout(function(){
			callback(boost); 
		},delay);
	};
	return module;
}).call({}, module);