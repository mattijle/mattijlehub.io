(function(module){
	'use strict';
	var items = [{
		icon: 'img/01d.png',
		name: 'Such Clicker',
		base: 0.2,
		price: 10,
		multiplier:1.25
	},{
		icon: 'img/13n.png',
		name: 'Doge Comrade',
		base: 1.0,
		price: 50,
		multiplier:1.25
	},{
		icon: 'img/01n.png',
		name: 'Such Sleepy',
		base: 10,
		price: 500,
		multiplier:1.25
	},{
		icon:'img/03d.png',
		name:'Ethereal Doge',
		base: 100,
		price: 5000,
		multiplier:1.35
	}];
	module.exports.get = function(callback){
		callback(items);
	};
	return module;
}).call({},module);