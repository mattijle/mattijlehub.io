(function(module){
	'use strict';
	var $ = require('jquery');
	var randomFromArray = function (arr) {           
		var randomIndex = Math.floor(Math.random() * arr.length);
		return arr[randomIndex];
	};
	var suchcolors = [ "#0066FF", "#FF3399", "#33CC33", "#8533FF", "#33D6FF", "#FF5CFF", "#19D1A3", "#FF4719", "#197519", "#6699FF", "#4747D1", "#D1D1E0", "#FF5050", "#CC99FF", "#66E0C2", "#FF4DFF", "#00CCFF"];
	var createInfo = function(txt) {
		var flash = $(window.document.createElement('span'));
		flash.css({
			position: 'absolute',
			left: Math.random()*75 +'%',
			top:Math.random()*75 +'%',
			fontSize:'24px',
			color: randomFromArray(suchcolors),
			textDecoration:  'none',
			cursor:'pointer'
		});
		flash.html(txt);
		return flash;
	};
	module.exports.suchcolors = suchcolors;
	module.exports.createInfo = createInfo;
	module.exports.randomFromArray = randomFromArray;
	return module;
}).call({}, module);