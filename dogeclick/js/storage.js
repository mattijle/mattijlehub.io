(function (module) {
	'use strict';
	var load = function(callback){
		var obj = JSON.parse(localStorage.getItem('DOGECLICKER-ss14-221x'));
		callback(obj);
	};
	var save = function(obj){
		localStorage.setItem('DOGECLICKER-ss14-221x',JSON.stringify(obj));
	};
	var reset = function(){
		localStorage.setItem('DOGECLICKER-ss14-221x',JSON.stringify(''));	
	};
	module.exports.reset = reset;
	module.exports.load = load;
	module.exports.save = save;
	return module;
}).call({},module);