/*global onmessage: true*/
/* global postMessage: false */
'use strict';
var upd;
onmessage = function(ev){
	clearInterval(upd);
	var ii = parseFloat(32*(ev.data/1000));
	upd = setInterval(function(){
		if(ev.data >0){
			postMessage(ii);
		}
	},32);
};