(function(module){
	'use strict';
	var win;
	var inventory = {};
	var items = require('./items');
	var boosts = require('./boosts');
	var storage = require('./storage');
	var util = require('./util');
	var _ = require('underscore');
	var $ = require('jquery');
	var dogeCount = 0;
	var clickBoost = 0;
	var baseClick = 1;
	var updateInterval;
	var dps_worker = new Worker('js/dps_task.js');
	var dogeCountEl = $('span[data-doge-count="doge"]');
	var dogesPerSecondEl = $('span[data-doges-per-second="doge"]');
	var upgradeColumn = $('div[data-doge-upgrades="doge"]');
	var inventoryColumn = $('div[data-doge-itams="doge"]');
	var boostColumn = $('div[data-doge-boosts="doge"]');
	

	var addInventoryItem = function(item){
		var ii = inventoryColumn.find('#wowinventory');
		var icon = $(win.document.createElement('img'));
		icon.addClass('smallicon');
		icon.css({
			top: Math.random()*100+'%',
			left: Math.random()*90+'%'
		});
		icon.attr('src',inventory[item].icon).load(function(){
			ii.append(icon);	
		});
	};
	var processBoost = function(el){
		var name = $(el).attr('data-name');
		var action = $(el).attr('data-action');
		var value = $(el).attr('data-value');
		var duration = $(el).attr('data-duration');
		switch(action){
			case 'extra':
				dogeCount *= value;
				break;
			case 'click': 
				if(duration > 0){
					var old = clickBoost;
					win.setTimeout(function(){
						clickBoost = old;
					},duration*1000);
				}
				clickBoost += parseFloat(value);
				break;
		}
		var info = util.createInfo(name);
		win.setTimeout(function(){
			info.remove();
		},15000);
		$('.such.overlay').append(info);
	};
	var registerHandlers = function () {
		$('a.reset').on('click',function(e){
			e.preventDefault();
			storage.reset();
			win.location.reload();
			return false;
		});
		$('a[data-doge-clicker="doge"]').on('click',function(e){
			e.preventDefault();
			dogeCount += parseFloat(baseClick+(baseClick*clickBoost));
			dogeCountEl.html(dogeCount.toFixed(2)+' doges');
			return false;
		});
		$('a.choose').on('click',function(){
			switch ($(this).attr('data-show')){
				case 'boost':
					upgradeColumn.hide();
					boostColumn.show();
					break;
				case 'upgrade':
					upgradeColumn.show();
					boostColumn.hide();
					break;
			}
		});
		dps_worker.onmessage = function(ev){
			dogeCount += ev.data;
		};
		$('.upgrade').on('click',function(e){
			e.preventDefault();
			var price = $(this).attr('data-price');
			if(dogeCount >= price){
				var item = $(this).attr('data-name');
				addInventoryItem(item);			
				inventory[item].count++;
				dps_worker.postMessage(calculateDPS());
				dogeCount -= parseFloat(price);
				var newPrice = parseFloat(price*inventory[item].multiplier);
				$(this).attr('data-price', newPrice.toFixed(0));
				$(this).find('.price').html(newPrice.toFixed(0));
				registerTimers();
			}
			return false;
		});
	};
	var calculateDPS = function(){
		var m = _.map(inventory,function(i){
			var k = i.count*(i.base + (i.base*i.boost));
			return k;
		});
		return _.reduce(m,function(memo,num){
			return memo + num;
		},0);
	};
	var registerTimers = function(){
		var saveInterval = win.setInterval(function(){
			save();
		},10000);
	};
	var createUpgradeMenu = function(){
		items.get(function(data){
			_.each(data,function(e){
				if(!inventory[e.name]){
					inventory[e.name] = {
						count: 0, 
						base: e.base,
						multiplier:e.multiplier,
						icon:e.icon,
						boost:0
					};	
				}
				inventory[e.name].base = e.base;
				inventory[e.name].boost = 0;
				var price = e.price*Math.pow(inventory[e.name].multiplier,inventory[e.name].count);
				var sp = win.document.createElement('div');
				sp.setAttribute('class','upgrade');
				sp.setAttribute('data-price', price.toFixed(0) );
				sp.setAttribute('data-name', e.name);
				sp.setAttribute('data-icon', e.icon);
				sp.setAttribute('data-multiplier', e.multiplier);
				sp.setAttribute('data-base', e.base);
				sp.innerHTML = 
				'<span class="icon"><img src="'+e.icon+'" class="icon">'+
					'</span><span class="name">'+e.name+
					'</span><span class="price">'+price.toFixed(0)+'</span>';
				upgradeColumn.append(sp);
				upgradeColumn.append(win.document.createElement('br'));
			});
		});
	};
	var createInventory = function(){
		var ii = inventoryColumn.find('#wowinventory');
		_.each(inventory,function(item,e){
			for(var i=0;i<parseInt(item.count);i++){
				addInventoryItem(e);	
			}	
		});		
	};
	var render = function(){
		dogeCountEl.html(dogeCount.toFixed(2)+' doges');
		dogesPerSecondEl.html(calculateDPS().toFixed(2) + ' doges per second.');
	};
	var mainLoop = function(){
		win.setInterval(render,1);
		dps_worker.postMessage(calculateDPS());
	};
	var save = function(){
		var obj = {
			baseClick: baseClick,
			clickBoost: clickBoost,
			dogeCount: dogeCount,
			inventory: inventory		
		};
		storage.save(obj);
	};
	var rotateBoosts = function(b){
		console.log('Calling rotate ',b);
		var such = $(window.document.createElement('a'));
		such.css({
			position: 'absolute',
			left: Math.random()*75 +'%',
			top:Math.random()*75 +'%',
			fontSize:Math.max(16, Math.random() * 24) +'px',
			color: util.randomFromArray(util.suchcolors),
			textDecoration:  'none',
			pointerEvents:'all',
			cursor:'pointer'
		});
		such.addClass('clicktheboost');
		such.attr('data-action',b.action);
		such.attr('data-duration',b.duration);
		such.attr('data-value',b.value);
		such.attr('daya-name', b.name);
		such.html('So Boost!');
		such.on('click',function(e){
			e.preventDefault();
			processBoost(this);
			$(this).remove();
			return false;
		});
		win.setTimeout(function(){
			such.remove();
		},10000);
		$('.such.overlay').append(such);
		boosts.generate(rotateBoosts);
	};
	var createOverLay = function(){
		$('body').append('<div class="such overlay"/>')
				.children(".such.overlay")
				.css({position: "fixed",
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            pointerEvents: "none"});
	};
	module.exports = function(w) {
		win = w;
		storage.load(function(d){
			if(d !== null){
				baseClick = (d.baseClick)?d.baseClick:1;
				clickBoost = (d.clickBoost)?d.clickBoost:0;
				dogeCount = (d.dogeCount)?d.dogeCount:0;
				inventory = (d.inventory)?d.inventory:{};	
			}
			dogeCountEl.css('color',util.randomFromArray(util.suchcolors));
			dogesPerSecondEl.css('color',util.randomFromArray(util.suchcolors));
			createUpgradeMenu();
			createInventory();
			createOverLay();
			registerHandlers();
			registerTimers();
			boosts.generate(rotateBoosts);
			mainLoop();
		});
	};
	return module;
}).call({},module);